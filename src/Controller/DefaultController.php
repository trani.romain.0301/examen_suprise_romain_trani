<?php

namespace App\Controller;

use App\Model\DefaultModel;
use Core\Kernel\AbstractController;
use App\Service\Form;
use App\Service\Validation;


/**
 *
 */
class DefaultController extends AbstractController
{

    public function index()
    {
        $message = 'Bienvenue sur le framework MVC';
        //$this->dump($message);
        $this->render('app.default.frontpage',array(
            'message' => $message,
        ));
    }

    // contact
    public function contact() {
        $message1 = 'Contact';
        $errors = array();
        if(!empty($_POST['submitted'])) {
            // Faille XSS.
            $post = $this->cleanXss($_POST);
            // Validation
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                DefaultModel::insert($post);
                // Message flash
                $this->addFlash('success', 'Merci pour votre message!');
                // redirection
                $this->redirect('home');
            }
        }
        $form = new Form($errors);
        $this->render('app.default.contact', array(
            'form' => $form,
            'message1' => $message1
        ));
    }

    public function listing()
    {
        $listing1 = 'Listing';
        $this->render('app.default.listing',array(
            'contact' => DefaultModel::getAllDefaultOrderBy(),
            'listing1' => $listing1,
        ));
    }






    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }


    private function validate($v,$post)
    {
        $errors = [];
        $errors['sujet'] = $v->textValid($post['sujet'], 'sujet',3, 150);
        $errors['email'] = $v->emailValid($post['email'], 'email',5, 150);
        $errors['message'] = $v->textValid($post['message'], 'message');
        return $errors;
    }

    private function getDefaultByIdOr404($id)
    {
        $contact = DefaultModel::findById($id);
        if(empty($contact)) {
            $this->Abort404();
        }
        return $contact;
    }



}
